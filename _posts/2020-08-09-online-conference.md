---
layout: post
title: "راهنمای برگزاری همایش برخط نامتمرکز"
cover: "2020-08-09/cover.png"
categories: [Free Culture]
tags: [online, meeting, seminar, conference]
---


حال که همه‌گیری ویروس کرونا، این توفیق اجباری را فراهم کرده که گردهمایی‌ها به صورت برخط برگزار شوند، باید تلاش کنیم که محدودیت‌های دنیای واقعی را کورکورانه در جهان برخط نیز به دوش نکشیم و تا جای ممکن، از این محددیت‌ها بکاهیم. در ادامه، روشی برای برگزاری همایش‌هایی چون روز آزادی نرم‌افزار پیشنهاد می‌شود که البته، به دلیل تازه بودن این مفهوم، خام‌دستانه بوده و در آینده، با تفکر بیش‌تر و ورود تجربیات تازه، به‌روز خواهد شد. همرسانی نظرها، پیشنهادها و نقدها به این موضوع، بی شک موجب رشد و بهبود این طرح و کمک به اجتماع‌هایی نظیر اجتماع کاربری نرم‌افزار آزاد خواهد شد.

روند برگزاری همایش به دو بخش پیش از همایش و روز نمایش تقسیم می‌شود.

پیش از همایش
============
آن‌چه که پیش از همایش لازم است، سکویی برای انتشار آزاد پرونده‌های ویدیویی است. این سکو می‌تواند نمونه‌ای از [پیرتیوب](https://joinpeertube.org) یا هر فناوری مناسب دیگری باشد. در صورت امکان، حتا می‌توان از یک کانال، روی نمونه‌ای از پیرتیوب استفاده کرد و ویدیوها را آن‌جا بازنشر داد؛ هرچند این کار، از خودکار بودن روند برگزاری پس از مشخّص شدن مکان بارگزاری می‌کاهد، ولی نیاز به تمرکز در ابتدای کار را از بین می‌برد.

با اعلام سکوی مشخّص‌شده در مدّت زمانی معقول پیش از روز نمایش، از افراد خواسته می‌شود ارائه‌های خود را به صورت ویدیویی ضبط کرده و پس از اطمینان از محتوای ارائه‌شده، آن را روی سکوی مورد نظر بارگذاری کنند.

با آغاز از زمان مشخّص‌شده برای شروع همایش، هر ویدیوی بارگذاری شده، با یک گام زمانیِ از پیش تعیین‌شده، مدّت زمانی برای پرسش و پاسخ مربوط به آن ارائه برایش مشخّص می‌شود. برای مثال، نخستین ویدیوی بارگذاری شده، ساعت ۱۰، دومی ساعت ۱۰:۳۰، سومی ساعت ۱۱ و همین‌طور تا پایان ارائه‌ها، زمان‌ها به صورت خودکار برایشان تخصیص داده می‌شود.

مخاطبین می‌توانند در فاصلهٔ بین بارگذاری ارائه و روز همایش، ارائه‌ها را مشاهده کرده و در صورت نیاز، نکات مربوط به آن را یادداشت کرده و زمان شروع جلسهٔ پرسش و پاسخ آن را محاسبه کنند.

روز همایش
=========
در روز همایش، در ساعت اعلام شده، اتاق گفت‌وگو که می‌تواند نمونه‌ای از [بیگ‌بلوباتن](https://bigbluebutton.org) باشد، گشوده شده و نخستین ارائه‌دهنده، آمادهٔ پاسخ‌گویی به پرسش‌ها و نکات مخاطبان است. پس از پایان زمان تخصیص‌یافته، ارائه‌دهندهٔ بعدی، اتاق را در دست گرفته و به پرسش‌های مخاطبان خود پاسخ می‌دهد. این روند تا پایان همایش ادامه خواهد یافت.

مزیت‌ها
======
 * نداشتنن تمرکز و جلوگیری از کند شدن روند برگزاری.
 * نبودن فرایند داوری و گزینش ارائه‌ها و عدم دخالت سلیقه.
 * مشخّص بودن برنامهٔ زمان‌بندی به صورت خودکار از پیش.
 * وجود امکان ویرایش و رفع مشکلات برای ارائه‌دهنده.
 * وجود زمان برای دیدن ارائه‌ها، فکر کردن روی آن‌ها و رسیدن به پرسش.
 * امکان گزینش و حضور فقط در پرسش و پاسخ مورد نظر فرد.
 * نبودن محدودیت زمانی برای پرسش و پاسخ هر تعداد ارائه.
 
معایب
=====
هنوز معایبی برای این مدل برگزاری پیدا نشده است. خوشحال می‌شوم با نظرات خود، این نوشته را کامل‌تر کنید.
